﻿using System;

namespace StdioIPC {

    public interface IReceiver {
        event Action<IConnection,Object> OnRecieve;
        void Register<T>( Action<IConnection,T> onReceive );
        void Receive( IConnection conn, Object packet );
    }

    public delegate void ReceiveFn( IConnection conn, Object packet, int sz );

    public interface IConnection {
        IProtocol Protocol { get; set; }
        ReceiveFn Receiver { get; set; }

        bool Connected { get; set; }
        int Send( Object obj );
    }
	
    public interface IProtocol {
        int Save( Object obj, Char[] buf_, int index, int size_ );
        int Load( out Object obj, Char[] buf_, int index, int size_ );
        int Save( Object obj, byte[] buf_, int index, int size_ );
        int Load( out Object obj, byte[] buf_, int index, int size_ );
    }

}
