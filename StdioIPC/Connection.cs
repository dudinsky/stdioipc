﻿using System;
using System.Threading;
using System.IO;
using StdioIPC.Common;

namespace StdioIPC {

    public abstract class ConnectionBase : IConnection {
        protected Thread clientThread;

        /// IConnection interface implementation
        public IProtocol Protocol { get; set; }
        public ReceiveFn Receiver { get; set; }

        protected bool connected_ = false;
        public bool Connected {
            get { return connected_; }
            set {
                if( value != connected_ ) {
                    if( value ) Start(); else Stop();
                    connected_ = value;
                }
            }
        }

        protected abstract void Listen();
        public abstract int Send(Object msg);

        protected virtual void Stop() {
            DEBUG.Assert( connected_ );
            connected_ = false;
            if( clientThread != null )
                clientThread.Join();
            clientThread = null;
        }
        protected virtual void Start() {
            DEBUG.Assert( !connected_ );
            DEBUG.Assert( clientThread==null );
            connected_ = true;
            clientThread = new Thread( new ThreadStart( Listen ) );
            clientThread.Start();
            //clientStream = client.GetStream();
            //Log.Info( "New client started at ...\n" );
        }

    }

    
    /*
    public class Connection : ConnectionBase {
        protected Stream ostream_;
        protected Stream istream_;
        protected byte[] ibuff_ = new byte[4*1024];
        protected byte[] obuff_ = new byte[2*1024];

        private const int HeaderSize = 2;
        private const int FooterSize = 2;

        public static void SaveInt( int val, byte[] buff_, int index ) {
            buff_[index+0] = (byte)(val);
            buff_[index+1] = (byte)(val>>8);
            DEBUG.Assert( (val>>16)==0 );
        }
        public static void LoadInt( out int val, byte[] buff_, int index ) {
            val = ( ( (int)( buff_[index+1] ) )<<8 ) + ((int)(buff_[index+0]));
        }

        public Connection( Stream istream, Stream ostream ) {
            //istream_ = Stream.Synchronized(istream);
            //ostream_ = Stream.Synchronized(ostream);
            istream_ = istream;
            ostream_ = ostream;
        }

        public override int Send( Object msg ) {
            int sz = 0;
            if (ostream_ != null) {
                sz = Protocol.Save(msg, obuff_, HeaderSize, obuff_.Length - HeaderSize - FooterSize);
                SaveInt(sz, obuff_, 0);
                SaveInt(sz, obuff_, sz + HeaderSize);
                sz += ( HeaderSize + FooterSize );
                ///!!FIXME 
                ///!!FIXME await ostream_.WriteAsync(obuff_, 0, sz + HeaderSize + FooterSize);
                
                ostream_.Write( obuff_, 0, sz );
                
                ///!!ostream_.Flush();
            }
            return sz;
        }

        protected override void Listen() {
            int inBuffer = 0;
            ///!!FIXME Task<int> aReadTask = null;
            ///!!FIXME CancellationTokenSource cs = null;
            int bytesRead = 0;
            while( connected_ ) {
                try {
                    bytesRead = istream_.Read( ibuff_, inBuffer, ibuff_.Length-inBuffer );    
                }
                catch {
                    //a socket error has occured
                    break;
                }
                if( bytesRead==0 ) {
                    //the client has disconnected from the server
                    Thread.Sleep( 1 );
                    continue;
                }

                if( bytesRead==0 ) {
                    Thread.Sleep(1);
                    continue;
                }
                inBuffer += bytesRead;
                if( inBuffer<=HeaderSize+FooterSize )
                    continue;
                int index = 0;
                for (;;) {
                    int packetSize = 0;
                    LoadInt( out packetSize, ibuff_, index );
                    int fullSize = packetSize + HeaderSize + FooterSize;
                    if( inBuffer < index + fullSize )
                        break;
                    Object packetRead = null;
                    int packetSize2 = -1;
                    LoadInt( out packetSize2, ibuff_, index+HeaderSize+packetSize );
                    DEBUG.Assert( packetSize2==packetSize );
                    int sz = Protocol.Load( out packetRead, ibuff_, index+HeaderSize, packetSize );
                    DEBUG.Assert( packetSize==sz );
                    Receiver( this, packetRead, fullSize );
                    index += fullSize;
                }
                inBuffer -= index;
                for( int i = 0; i < inBuffer; i++ )
                    ibuff_[i] = ibuff_[i+index];
                DEBUG.Assert( inBuffer >= 0 );
                bytesRead = 0;
            }
        }

        protected override void Stop() {
            ostream_.Flush();
            ostream_.Close();
            base.Stop();
        }
    }
    */



    public abstract class TextConnection : IConnection {
        protected TextWriter writer_;
        protected char[] buff = new char[1024];

        public IProtocol Protocol { get; set; }
        public ReceiveFn Receiver { get; set; }
        protected const String Prefix = "PACKET=";
        protected const String ExitPacketQuery = "!!IPC-DISCONNECT-QUERY";
        protected const String ExitPacketReply = "!!IPC-DISCONNECT-REPLY";

        public event EventHandler OnDisconnected;
        public event Action<String> OnDataReceived;

        protected bool connected_=false;
        public bool Connected {
            get { return connected_; }
            set {
                if( value!=connected_ ) {
                    if( value )
                        Connect();
                    else
                        Close();
                }
            }
        }
        public int Send( Object obj ) {
            if( !connected_ || writer_==null )
                return 0;
            int sz = Protocol.Save( obj, buff, Prefix.Length, buff.Length-Prefix.Length );
            Prefix.CopyTo( 0, buff, 0, Prefix.Length );
            writer_.WriteLine( buff, 0, sz+Prefix.Length );
            //System.Diagnostics.Debug.WriteLine( new String( buff, 0, sz+Prefix.Length ) );
            return sz;
        }

        protected void SendString( String toSend ) {
            if( writer_!=null )
                writer_.WriteLine( toSend );
        }

        public abstract bool Connect();

        public TextConnection( IProtocol protocol, TextWriter writer = null ) {
            Protocol = protocol;
            writer_ = writer;
        }

        protected virtual bool onDataRecived( String receivedLine ) {
            if( String.IsNullOrEmpty( receivedLine ) )
                return true;
            if( OnDataReceived!=null )
                OnDataReceived( receivedLine );
                //System.Diagnostics.Debug.WriteLine( receivedLine );
            if( receivedLine.StartsWith( Prefix, StringComparison.InvariantCulture ) ) {
                Object packet;
                char[] chars = receivedLine.ToCharArray();
                int readSize = Protocol.Load( out packet, chars, Prefix.Length, chars.Length-Prefix.Length );
                Receiver( this, packet, readSize );
            }

            return true;
        }

        public virtual void Close() {
            if( writer_!=null )
                writer_.Flush();
            connected_ = false;
            if( OnDisconnected!=null )
                OnDisconnected( this, EventArgs.Empty );
        }
    }

    public class StdioConnection : TextConnection {
        TextReader reader_;
        Thread ListenThread;

        public StdioConnection( IProtocol protocol, TextWriter writer, TextReader reader ) 
            : base( protocol, writer ) {
            reader_ = reader;
        }

        private void Listen() {
            try {
                while (connected_) {
                    String str = reader_.ReadLine();

                    if( !onDataRecived(str) )
                        break;

                    Thread.Sleep(1);
                }
            }
            catch (ThreadAbortException ex) {
                Thread.ResetAbort();                
            }
            catch( Exception ex) {
                do ; while (false);
            }

            ListenThread = null;
            if( connected_ ) {
                Close();
            }
            connected_ = false;
        }

        public override void Close() {
            if( connected_ ) {
                SendString(ExitPacketQuery);
                connected_ = false;
                if( ListenThread!=null )
                    ListenThread.Abort();
                ListenThread=null;
                base.Close();
            }
        }

        protected override bool onDataRecived( String receivedLine ) {
            if( receivedLine == ExitPacketReply ) {
                return false;
            }
            base.onDataRecived(receivedLine);
            return true;
        }

        public override bool Connect() {
            if( !connected_  ) {
                connected_ = true;
                ListenThread = new Thread( Listen );
                ListenThread.Start();
            }
            return connected_;
        }

    }

}

