﻿using System;
//using System.Collections;
//using System.Collections.Generic;


namespace StdioIPC.Common {

    public static class Extras
    {
        //public static void Resize<T>( this List<T> list, int sz, T c = default(T) ) {
            //int cur = list.Count;
            //if(sz < cur)
                //list.RemoveRange(sz, cur - sz);
            //else if(sz > cur) {
                //if(sz > list.Capacity)//this bit is purely an optimisation, to avoid multiple automatic capacity changes.
                  //list.Capacity = sz;
                //for( int K=sz-cur; --K>=0; ) list.Add( c );
                ////list.AddRange(Enumerable.Repeat(c, sz - cur));
            //}
        //}
        //public static void Resize<T>( this List<T> list, int sz, Func<T> func ) {
            //int cur = list.Count;
            //if(sz < cur)
                //list.RemoveRange(sz, cur - sz);
            //else if(sz > cur) {
                //if(sz > list.Capacity)//this bit is purely an optimisation, to avoid multiple automatic capacity changes.
                  //list.Capacity = sz;
                //for(int K=sz-cur; --K>=0; ) list.Add( func() );
                ////list.AddRange(Enumerable.Repeat(0, sz-cur).Select(x=>func()));
            //}
        //}

        //public static IList<T> AddRange<T>( this IList<T> list, IEnumerable<T> range ) {
            //foreach( var r in range ) 
                //list.Add( r );
            //return list;
        //}

        //public static void ForEach<T>( this IEnumerable<T> source, Action<T> action ) {
            ////source.ThrowIfNull( "source" );
            ////action.ThrowIfNull( "action" );
            //foreach( T element in source ) {
                //action( element );
            //}
        //}

        public static Type GetType( string typeName ) {
            var type = Type.GetType( typeName );
            if( type != null ) return type;
            foreach( var a in AppDomain.CurrentDomain.GetAssemblies() ) {
                type = a.GetType( typeName );
                if( type != null )
                    return type;
            }
            return null;
        }

        public static T NewInstance<T>( string typeName ) where T : class {
            T result = null;
            Type theType = GetType( typeName );
            if( theType!=null )
                result = Activator.CreateInstance( theType ) as T;
            return result;
        }
    }

    //public class WrapValue<T> where T : struct {
        //public T Value { get; set; }
        //public WrapValue( T value ) { this.Value = value; }
    //}
	
    //public class WrapValueObservable<T> where T : struct {
		//public Action<T> OnChanged;
		//public T value_;
		//public T Value { get { return value_; }  set { value_=value; if(OnChanged!=null) OnChanged(value); } }
        //public WrapValueObservable( T value ) { this.Value = value; }
    //}

}
