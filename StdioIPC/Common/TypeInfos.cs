﻿using System;
using System.Collections.Generic;

namespace StdioIPC.Common {

    class TypeInfos<Info> where Info : class,new() {
        private Dictionary<Type,Info> Infos = new Dictionary<Type, Info>();
        public Info DefaultIfNotFound { get; private set; }

        public TypeInfos( Info defaultIfNotFound = null ) { DefaultIfNotFound=defaultIfNotFound; }


        public Info TryGet( Type type ) { Info res; return Infos.TryGetValue( type, out res )? res : DefaultIfNotFound; }
        public Info TryGet<T>() { return TryGet( typeof( T ) ); }
        public Info FindOrInsert<T>() { Info res; if( !Infos.TryGetValue( typeof( T ), out res ) ) { res = new Info(); Infos.Add( typeof( T ), res ); } return res; }
        public Info this[Type type] { get { return TryGet(type); } }
        public bool Contains<T>() { return Infos.ContainsKey( typeof( T ) ); }
        public void Register<T>() { if( !Infos.ContainsKey( typeof( T ) ) ) Infos.Add( typeof( T ), new Info() ); }
    }

}
