﻿using System;
using System.Collections.Generic;
using StdioIPC.Common;

namespace StdioIPC {

    public abstract class Server /*: IConnection*/ {
        public IProtocol Protocol { get; set; }
        public IReceiver Receiver { get; set; }

        public event Action<IConnection> OnClientConnected;

        protected List<IConnection> Clients = new List<IConnection>();

        public int Send( Object packet ) {
            int sz = 0;
            foreach( IConnection client in Clients ) {
                sz += client.Send( packet );
            }
            return sz;
        }

        public bool HasClients { get { return Clients.Count>0; } }

        protected Server() {
            Receiver = new Receiver();
        }

        protected virtual void Start() {
            DEBUG.Assert( Protocol!=null && Receiver!=null );
            DEBUG.Assert( !connected_ );
            connected_=true;
            foreach( IConnection client in Clients ) {
                client.Connected = true;
            }
        }
        protected virtual void Stop() {
            DEBUG.Assert( connected_ );
            connected_ = false;
            foreach( IConnection client in Clients ) {
                client.Connected = false;
            }
        }

        private bool connected_ = false;
        public bool Connected {
            get { return connected_; }
            set {
                if (value != connected_) {
                    if( value ) Start(); else Stop(); 
                }
                    
            }
        }

        private void ReceiveFn( IConnection conn, Object packet, int sz ) {
            //bytesReceived += sz;
            Receiver.Receive( conn, packet );
        }

        protected virtual void InsertConnection_( IConnection conn ) {
            DEBUG.Assert( Protocol!=null && Receiver!=null );
            conn.Protocol = Protocol;
            conn.Receiver = ReceiveFn;
            Clients.Add( conn );
            conn.Connected = Connected;
            if( OnClientConnected != null )
                OnClientConnected( conn );
        }

        protected virtual bool RemoveConnection_( IConnection conn ) {
            if( Clients.Remove( conn ) ) {
                conn.Receiver = null;
                conn.Protocol = null;
                conn.Connected = false;
                return true;
            }
            return false;
        }

        public abstract bool InitClient( Object client );
        public abstract IConnection AddClient( Object client );
    }

}
