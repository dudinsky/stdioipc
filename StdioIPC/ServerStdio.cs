﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using StdioIPC.Common;
using System.Diagnostics;
using System.Security.Cryptography;

namespace StdioIPC {

    public class ProcessConnection : TextConnection {
        private Process process ;
        private EventHandler onExitHandler;
        private DataReceivedEventHandler onDataHandler;

        public ProcessConnection( IProtocol protocol, Process process )
            : base( protocol ) {
            if( !InitProcessBeforeStart( process ) )
                throw new ArgumentException();
        }
        
        public override void Close() {
            if( connected_ ) {
                process.CancelOutputRead();
                base.Close();
            }
        }

        public static bool IsRunning( Process process ) {
            try { Process.GetProcessById( process.Id ); }
            catch( InvalidOperationException ) { return false; }
            catch( ArgumentException ) { return false; }
            return true;
        }

        public bool InitProcessBeforeStart( Process process ) {
            if( this.process==process )
                return true;
            if( IsRunning( process ) || this.process!=null )
                return false;
            this.process = process;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardInput = true;//!!FIXME
            process.StartInfo.RedirectStandardOutput = true;
            process.EnableRaisingEvents = true;
            onExitHandler = ( sender, args ) => OnClientExited( sender as Process );
            process.Exited += onExitHandler;
            onDataHandler = new DataReceivedEventHandler( ( sendingProcess, outLine ) => onDataRecived( outLine.Data ) );
            process.OutputDataReceived += onDataHandler;
            return true;
        }

        void OnClientExited( Process pp ) {
            DEBUG.Assert( pp!=null );
            DEBUG.Assert( pp==process );
            pp.Exited -= onExitHandler;
            pp.OutputDataReceived -= onDataHandler;
            Close();
        }

        public void OnProcessRunning( Process process ) {
            process.BeginOutputReadLine();
            connected_ = true;
            if( process.StartInfo.RedirectStandardInput )
                writer_ = process.StandardInput;
        }

        public override bool Connect() {
            if( !connected_ &&  process!=null && IsRunning( process )  ) 
                OnProcessRunning( process );
            return connected_;
        }


        protected override bool onDataRecived(String receivedLine) {
            if (receivedLine == ExitPacketQuery ) {
                SendString(ExitPacketReply);
                return false;
            }
            base.onDataRecived( receivedLine );
            return true;
        }

        //public static ConnectionStdio Connect( IProtocol protocol, Process process ) {
            //if( IsRunning( process ) {
            //ConnectionStdio conn = new ConnectionStdio( protocol, process );
            //return conn;
            //}
            //return null;
            //}
        }

    /*
    public class ServerStdio : Server {
        //private static readonly ClientInfos Infos = new ClientInfos();
        //EventHandler GetOnExitedHandler( MyConnection conn ) {
            //EventHandler handler = ( sender, args ) => OnClientExited( sender as Process, conn );
            //return handler;
        //}
        public ServerStdio( IProtocol protocol ) {
            Protocol = protocol;
            Start();            
        }

        public static bool IsRunning( Process process ) {
            try { Process.GetProcessById( process.Id ); }
            catch( InvalidOperationException ) { return false; }
            catch( ArgumentException ) { return false; }
            return true;
        }

        public override IConnection AddClient( object client ) {
            Process process = client as Process;
            if (process != null) {
                if (!IsRunning(process)) {
                    process.StartInfo.RedirectStandardInput = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.UseShellExecute = false;
                    try {
                        process.Start();
                    }
                    catch (Exception ex ) {
                        //Console.WriteLine( "=== Error Client Connected {0}", ex.Message );
                        //p.Dispose();
                        //Logger.Do( x => x.Log( ex.LogMessage(), Category.Exception, Priority.High ) );
                        return null;
                    }
                }

                process.EnableRaisingEvents = true;
                IConnection conn = new Connection( process.StandardOutput.BaseStream, process.StandardInput.BaseStream );
                EventHandler handler = ( sender, args ) => OnClientExited( sender as Process, conn );
                process.Exited += handler;
                InsertConnection_( conn );
                Infos.Add( new ClientInfo( process, handler ) );
                return conn;
            }
            return null;
        }

        protected override void Stop() {
            var clients = new ClientInfos( Infos );
            foreach( var cc in clients ) {
                cc.pp.Kill();
                cc.pp.WaitForExit();
            }
            clients.Clear();
            base.Stop();
        } 
    }
    */

}
