﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace StdioIPC.Protocols {
    public class BinaryFormatterProtocol : IProtocol {
        BinaryFormatter Formatter = new BinaryFormatter();
        byte[] bytebuf;
        public int Save( Object obj, Char[] buf_, int index, int size_ ) {
            if( bytebuf==null ) 
                bytebuf = new byte[1024];
            int bsz = Save( obj, bytebuf, 0, bytebuf.Length );
            int asz = Convert.ToBase64CharArray( bytebuf, 0, bsz, buf_, index );
            return asz;
        }

        public int Load( out Object obj, Char[] buf_, int index, int size_ ) {
            byte[] bytebuf = Convert.FromBase64CharArray( buf_, index, size_ );
            Load( out obj, bytebuf, 0, bytebuf.Length );
            return bytebuf.Length;
        }

        public int Load( out Object obj, byte[] bytes, int index, int size ) {
            using( MemoryStream memstream = new MemoryStream( bytes, index, size ) ) {
                long posBefore = memstream.Position;
                obj = Formatter.Deserialize( memstream );
                long posAfter = memstream.Position;
                return (int)( posAfter-posBefore );
            }
        }
        public int Save( Object obj, byte[] bytes, int index, int size ) {
            using( MemoryStream memstream = new MemoryStream( bytes, index, size ) ) {
                long posBefore = memstream.Position;
                Formatter.Serialize( memstream, obj );
                long posAfter = memstream.Position;
                return (int)( posAfter-posBefore );
            }
        }
    }
}
