﻿using System;
using StdioIPC.Common;

namespace StdioIPC {

    public class Receiver : IReceiver {

        public class TypeInfo {
            public event Action<IConnection,Object> OnRecieve;

            public TypeInfo() { }
            public TypeInfo( Action<IConnection, Object> action ) { OnRecieve += action; }

            public void Recieve( IConnection connection, Object packet ) {
                DEBUG.Assert( OnRecieve!=null );
                OnRecieve( connection, packet );
            }
        }
        TypeInfos<TypeInfo> Infos = new TypeInfos<TypeInfo>();

        public event Action<IConnection,Object> OnRecieve;
        public void Register<T>( Action<IConnection, T> onReceive ) {
            TypeInfo info = Infos.FindOrInsert<T>();
            info.OnRecieve += ( client, packet ) => onReceive( client, (T)packet );
        }

        public void Receive( IConnection conn, Object packet ) {
            Type type = packet.GetType();
            TypeInfo info = Infos[type];
            if( info!=null )
                info.Recieve( conn, packet );
            else if( OnRecieve != null )
                OnRecieve( conn, packet );
            else {
                DEBUG.Assert( false );
                //Log.Info(" Client receive untracked packet: {0}", packet);
            }
        }

    }
}
