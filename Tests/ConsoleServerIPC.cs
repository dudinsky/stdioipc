﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using StdioIPC.Common;
using StdioIPC.Interfaces;

namespace StdioIPC {

    static class Program {

        static ConsoleKeyInfo WaitKey( String msg ) {
            Console.WriteLine( msg );
            while( !Console.KeyAvailable );
            ConsoleKeyInfo key = Console.ReadKey( true );
            return key;
        }

        static void RunClient( Server server ) {
            var p = new Process() {
                StartInfo = new ProcessStartInfo( "ConsoleClientIPC.exe" ),
            };
            server.AddClient( p );
        }


        static void Main( string[] args ) {
            Console.Title = "Simple IPC Server";
            
            String protocolName = ConfigurationManager.AppSettings["Protocol"]?? "StdioIPC.Protocols.TextProtocol";
            IProtocol Protocol = Extras.NewInstance<IProtocol>( protocolName );
            if( Protocol==null ) {
                Console.Write( "Failed to create protocol - {0}", protocolName );
                return;
            }
            Server server = new ServerStdio( Protocol );

            server.Receiver.OnRecieve += ( ( client, msg ) => {
                System.Diagnostics.Debug.WriteLine( msg );
                Console.WriteLine( "Receive message: {0}", msg );
            } );
            server.Receiver.Register<DateTime>( ( conn, time ) => Console.WriteLine( "Receive DateTime: {0}", time ) );
            server.Receiver.Register<int>( ( conn, id ) => Console.WriteLine( "Receive int: {0}", id ) );
            server.OnClientConnected += ( ( client ) => {
                Console.WriteLine( "=== New Client Connected" );
            } );


            Console.WriteLine( "Server start listening ...\n" );
            Console.WriteLine( "Press Enter to exit\n" );
            DateTime prevTime = DateTime.Now;

            RunClient( server );
            RunClient( server );
            RunClient( server );
            int NumPacket = 0;

            for( ; ; ) {
                Thread.Sleep( 1 );

                DateTime nowTime = DateTime.Now;
                if( ( nowTime-prevTime ).TotalSeconds>0.00001 ) {
                    prevTime = nowTime;
                    Console.WriteLine( " === Broadcast : {0} - {1}", NumPacket, nowTime );
                    server.Send( NumPacket++ );
                    server.Send( NumPacket++ );
                    server.Send( NumPacket++ );
                    Thread.Sleep( 1 );
                    server.Send( nowTime.ToString() ); NumPacket++;
                    server.Send( nowTime ); NumPacket++;
                }

                if( Console.KeyAvailable ) {
                    ConsoleKeyInfo key = Console.ReadKey( true );
                    break;
                }
            }

            Console.Write( "Server stopping\n" );
            server.Connected=false;
            WaitKey( "Exiting ... \n" );
        }
    }
}
