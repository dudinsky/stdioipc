﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using StdioIPC.Common;
using StdioIPC.Interfaces;

namespace StdioIPC {

    class Program {
        


        static void Main( string[] args ) {
            String protocolName = ConfigurationManager.AppSettings["Protocol"]?? "StdioIPC.Protocols.TextProtocol";
            IProtocol Protocol = Extras.NewInstance<IProtocol>( protocolName );
            if( Protocol==null ) {
                Console.Write( "Failed to create protocol - {0}", protocolName );
                return;
            }

            IConnection client = new ClientStdio( Protocol );
            client.Receiver.OnRecieve += ( ( conn, msg ) => {
                conn.Send( msg );
            } );
            client.Receiver.Register<string>( ( conn, msg ) => conn.Send( msg ) );
            client.Receiver.Register<DateTime>( ( conn, time ) => conn.Send( time ) );
            client.Receiver.Register<int>( ( conn, id ) => conn.Send( id ) );
            client.Connected = true;

            while( client.Connected ) {
                Thread.Sleep( 250 );
            }

            client.Connected = false;
        }
    }
}
